import os
import shutil
import sys
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.Qt import *
import sip
import cv2
import numpy as np
import json
import pymongo
from bson.objectid import ObjectId

sys.path.append(os.path.join(os.path.dirname(__file__), './lib'))
import initial_IrefTrim

class TestWindow(QMainWindow):
############################################################################################
    def __init__(self, parent=None):
        super(QMainWindow, self).__init__()
        self.parent = parent

        self.setWindowTitle('IrefTrim')
            
        self.result_info = {
            'N_pad'             :4,
            'choices'           :[0,1],
            'bare_name'         :self.parent.info_dict['bare_module'],
            'bare_code'         :self.parent.info_dict['bare_module_code'],
            'FE_version'        :self.parent.info_dict['FE_version'],
            'N_chip'            :self.parent.info_dict['chip_quantity'],
            'ComponentTypeCode' :self.parent.info_dict['typeCode'],
            'type             ' :self.parent.info_dict['type'],
            'value'             :{},
            'comment'           :''
        }
        for i in range(self.result_info['N_chip']):
            self.result_info['chip' + str(i+1)]=''

    def recieve_result(self,value_dict,comment):
        self.result_info['value'] = value_dict
        self.result_info['comment'] = comment

        self.fill_result()
        self.return_result()

    def fill_result(self):
        self.test_result_dict = {
            'results':{
                'localDB':{
                    'value'   :self.result_info['value'],
                    'comment' :self.result_info['comment']
                },
                'ITkPD' : {
                    'value'   :self.result_info['value'],
                    'comment' :self.result_info['comment']
                },
                'summary' : {
                    'value'   :self.result_info['value'],
                    'comment' :self.result_info['comment']
                }
            }
        }
        
        print('[Test Result] ' + str (self.test_result_dict['results']['localDB']['value'] ) )

###########################################################################################

    def init_ui(self):
        self.initial_wid = initial_IrefTrim.InitialWindow(self)
        self.update_widget(self.initial_wid)

    def scale_window(self,x,y):
        self.setGeometry(0, 0, x, y)

    def update_widget(self, w):
#        self.scale_window(510,425)
        self.scale_window(640,480)
        self.setCentralWidget(w)
        self.show()

    def close_and_return(self):
        self.close()
        self.parent.back_from_test()
       
    def back_page(self):
        self.parent.init_ui()

    def back_window(self):
        self.parent.recieve_backpage()
        
    def call_another_window(self,window):
        self.hide()
        window.init_ui()

    def return_result(self):
        self.parent.recieve_result(self,self.test_result_dict)
