import os
import sys
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.Qt import *
import sip

class InitialWindow(QWidget):
    def __init__(self, parent=None):
        super(QWidget, self).__init__(parent)
        self.parent = parent

        layout = QVBoxLayout()
        bottom_box = QHBoxLayout()
        grid_edit = QGridLayout()
        
        label_title = QLabel()
        label_title.setText('<center><font size="7">Input module property</font></center>')

        Next_button= QPushButton("&Next")
        Next_button.clicked.connect(self.pass_result)
        Back_button= QPushButton("&Back")
        Back_button.clicked.connect(self.back_page)

        bottom_box.addWidget(Back_button)
        bottom_box.addStretch()
        bottom_box.addWidget(Next_button)

        label_comment = QLabel()
        label_comment.setText('comment : ')
        
        self.edit_comment = QTextEdit()
        self.edit_comment.setText(self.parent.result_info['comment'])

        grid_edit.addWidget(label_comment,1,0)

        outer = QScrollArea()
        outer.setWidgetResizable(True)
        outer.setWidget(self.edit_comment)
        grid_edit.addWidget(outer,1,1)

        layout.addWidget(label_title)
        layout.addLayout( self.make_tab_layout() )
        layout.addStretch()
        layout.addLayout(grid_edit)
        layout.addLayout(bottom_box)
        
        self.setLayout(layout)

    def make_tab_layout(self):

        layout = QHBoxLayout()

        self.TabWidget  = QTabWidget()
        self.all_switch=[]

        ###############################################################
        ########             image file path                   ########
        if 'QUAD' in self.parent.result_info['ComponentTypeCode'] or 'practice' in self.parent.result_info['ComponentTypeCode']:
            if self.parent.result_info['FE_version'] in ['RD53A','practice','No FE chip']:
                image_file = '../image/Quad_RD53A.jpg'
            else:
                image_file =''
        elif 'TRIPLET' in self.parent.result_info['ComponentTypeCode'] and 'RING' in self.parent.result_info['ComponentTypeCode']:
            if self.parent.result_info['FE_version'] in ['RD53A','practice','No FE chip']:
                image_file = '../image/TRIPLET_RING_RD53A.jpg'
            else:
                image_file =''
        elif 'TRIPLET' in self.parent.result_info['ComponentTypeCode'] and 'STAVE' in self.parent.result_info['ComponentTypeCode']:
            if self.parent.result_info['FE_version'] in ['RD53A','practice','No FE chip']:
                image_file = '../image/TRIPLET_STAVE_RD53A.jpg'
            else:
                image_file =''
        else:
            image_file =''
        ###############################################################
            
        self.add_tab(self.make_standard_layout(image_file),self.TabWidget,'Bullet layout',self.all_switch)
        if os.path.exists( os.path.join(os.path.dirname(__file__),image_file) ):
            try:
                self.add_tab(self.make_geometrical_layout(image_file),self.TabWidget,'Geometrical layout',self.all_switch)
            except:
                pass
        else:
            pass
        layout.addWidget(self.TabWidget)
        return layout

    def add_tab(self,layout_tuple,TabWidget,tab_label,list_of_list):
        inner_layout = layout_tuple[0]
        radio_group_list = layout_tuple[1]
        
        tab = QWidget()
        tab.setLayout( inner_layout )
        
        list_of_list.append(radio_group_list)
        TabWidget.addTab(tab,tab_label)
        
    def make_standard_layout(self,image_file):
        layout= QHBoxLayout()
        radio_layout,radio_group_list = self.make_bullet()
        if 'QUAD' in self.parent.result_info['ComponentTypeCode'] or 'practice' in self.parent.result_info['ComponentTypeCode']:
            image_layout = self.make_image_layout( os.path.join(os.path.dirname(__file__),image_file),400,300 )
        if 'TRIPLET' in self.parent.result_info['ComponentTypeCode']:
            image_layout = self.make_image_layout( os.path.join(os.path.dirname(__file__),image_file),600,450 )
        layout.addStretch()
        layout.addLayout(radio_layout)
        layout.addStretch()
        layout.addLayout(image_layout)
        layout.addStretch()

        return (layout, radio_group_list)

    def make_geometrical_layout(self,image_file):
        if 'QUAD' in self.parent.result_info['ComponentTypeCode'] or 'practice' in self.parent.result_info['ComponentTypeCode']:
            layout ,radio_group_list= self.make_quad(image_file)
        if 'TRIPLET' in self.parent.result_info['ComponentTypeCode']:
            layout ,radio_group_list= self.make_triplet(image_file)
        else:
#            QMessageBox.warning(None, 'Warning',self.parent.result_info['type'] +' is not supported for geometrical layout.', QMessageBox.Ok)
            pass

        return (layout, radio_group_list)

    def make_image_layout(self,image_path,width,height):
        layout = QHBoxLayout()

        label = QLabel()
        pixmap = QPixmap(image_path)
        scaled_pixmap = pixmap.scaled(width,height,Qt.KeepAspectRatio,Qt.SmoothTransformation)
        label.setPixmap(scaled_pixmap)
        layout.addWidget(label)

        return layout

    def make_bullet(self):

        layout = QVBoxLayout()

        switch_list_list = []
        
        for i in range( self.parent.result_info['N_chip'] ):
            title_text='chip' + str(i+1) + ':'

            try:
                value_str = self.parent.result_info['value']['chip'+str(i)]
            except:
                value_str = '1000'
                
            switch_layout,switch_list = self.make_group(title_text,value_str)
            
            switch_list_list.append(switch_list)
            layout.addLayout(switch_layout)
        
        return layout, switch_list_list

    def make_quad(self,image_file):

        layout = QHBoxLayout()
        left_layout = QVBoxLayout()
        right_layout = QVBoxLayout()

        switch_list_list = []
        chip_layout_list = []

        for i in range( self.parent.result_info['N_chip'] ):
            title_text='chip' + str(i+1) + ':'
            try:
                value_str = self.parent.result_info['value']['chip'+str(i)]
            except:
                value_str = '1000'
            switch_layout,switch_list = self.make_group(title_text,value_str)

            switch_list_list.append(switch_list)
            chip_layout_list.append(switch_layout)

        image_layout = self.make_image_layout( os.path.join(os.path.dirname(__file__),image_file),400,300 )

        left_layout.addLayout(chip_layout_list[0])
        left_layout.addStretch()
        left_layout.addLayout(chip_layout_list[1])

        right_layout.addLayout(chip_layout_list[3])
        right_layout.addStretch()
        right_layout.addLayout(chip_layout_list[2])

        layout.addStretch()
        layout.addLayout(left_layout)
        layout.addLayout(image_layout)
        layout.addLayout(right_layout)
        layout.addStretch()

        return layout, switch_list_list

    def make_triplet(self,image_file):

        layout = QVBoxLayout()
        up_layout = QHBoxLayout()
        image_layout = QHBoxLayout()

        switch_list_list = []
        chip_layout_list = []

        for i in range( self.parent.result_info['N_chip'] ):
            title_text='chip' + str(i+1) + ':'
            try:
                value_str = self.parent.result_info['value']['chip'+str(i)]
            except:
                value_str = '1000'
            switch_layout,switch_list = self.make_group(title_text,value_str)

            switch_list_list.append(switch_list)
            chip_layout_list.append(switch_layout)

        image_layout.addStretch()
        image_layout.addLayout( self.make_image_layout( os.path.join(os.path.dirname(__file__),image_file),720,540 ) )
        image_layout.addStretch()

        up_layout.addLayout(chip_layout_list[0])
        up_layout.addStretch()
        up_layout.addLayout(chip_layout_list[1])
        up_layout.addStretch()
        up_layout.addLayout(chip_layout_list[2])

        layout.addStretch()
        layout.addLayout(up_layout)
        layout.addLayout(image_layout)

        return layout, switch_list_list

    def make_group(self,title_text,value_str):
        layout = QHBoxLayout()
        grid_layout = QGridLayout()

        switch_list = []
        
        msb = QLabel()
        msb.setText('MSB')
        msb.setAlignment(Qt.AlignBottom | Qt.AlignLeft)
        lsb = QLabel()
        lsb.setText('LSB')
        lsb.setAlignment(Qt.AlignBottom | Qt.AlignRight)
        decimal_title = QLabel()
        decimal_title.setText('Value')
        decimal_title.setAlignment(Qt.AlignBottom | Qt.AlignRight)

        grid_layout.addWidget(msb,0,1)
        grid_layout.addWidget(lsb,0,3)
        grid_layout.addWidget(decimal_title,0,4)

        title = QLabel()
        title.setText(title_text )
        grid_layout.addWidget(title,1,0)

        binaly_value = ''
        decimal_value = QLabel()
        decimal_value.setFrameStyle(QFrame.Box | QFrame.Plain)
        decimal_value.setAlignment(Qt.AlignVCenter | Qt.AlignRight)
        decimal_value.setFixedWidth(30)
        decimal_value.setFixedHeight(30)
        decimal_value.setStyleSheet("QLabel { background-color : linen; color : black; }");
        
        for i in range( self.parent.result_info['N_pad'] ):
            if value_str[i]=='1':
                switch_list.append( self.make_toggle_switch(True) )
            else:
                switch_list.append( self.make_toggle_switch(False) )
            layout.addWidget(switch_list[i])
            
        for i in range(len(switch_list)):
            switch_list[i].toggled.connect( lambda:self.change_text(switch_list,decimal_value) )
            binaly_value += str( int( switch_list[i].isChecked() ) )

        decimal_value.setText( str( int(binaly_value,2) ) )

        #        layout.addWidget(decimal_value)
        grid_layout.addLayout(layout,1,1,1,3)
        grid_layout.addWidget(decimal_value,1,4)

        return grid_layout, switch_list

    def change_text(self,switch_list,qlabel):
        binaly_value = ''
        for i in range(len(switch_list)):
            binaly_value += str( int( switch_list[i].isChecked() ) )
        qlabel.setText( str( int(binaly_value,2) ) )

    def make_toggle_switch(self,isON):
        switch = MySwitch()
        if isON:
            switch.setChecked(True)

        return switch

    def pass_result(self):
        try:
            result_dict = {}
            for i in range( self.parent.result_info['N_chip'] ):
                chipID = 'chip' + str(i+1)
                text = ''
                for j in range( self.parent.result_info['N_chip'] ):
                    text+= str( int( self.all_switch[ self.TabWidget.currentIndex() ][i][j].isChecked() ) )

                result_dict[chipID] = text

            free_comment = self.edit_comment.toPlainText()
        except:
            import traceback
            print (traceback.format_exc() )
            QMessageBox.warning(None, 'Warning','Please check the buttons.', QMessageBox.Ok)

        self.parent.recieve_result( result_dict , free_comment )
        
    def back_page(self):
        self.parent.close_and_return()

class MySwitch(QPushButton):
    def __init__(self, parent = None):
        super().__init__(parent)
        self.setCheckable(True)
        self.setMinimumWidth(44)
        self.setMinimumHeight(22)

    def paintEvent(self, event):
        label = "1" if self.isChecked() else "0"
        bg_color = Qt.green if self.isChecked() else Qt.white

        radius = 8
        width = 20
        center = self.rect().center()

        painter = QPainter(self)
        painter.setRenderHint(QPainter.Antialiasing)
        painter.translate(center)
        painter.setBrush(Qt.gray)

        pen = QPen(Qt.black)
        pen.setWidth(2)
        painter.setPen(pen)

        painter.drawRoundedRect(QRect(-width, -radius, 2*width, 2*radius), radius, radius)
        painter.setBrush(QBrush(bg_color))
        sw_rect = QRect(-radius, -radius, width + radius, 2*radius)
        if not self.isChecked():
            sw_rect.moveLeft(-width)
        painter.drawRoundedRect(sw_rect, radius, radius)
        painter.drawText(sw_rect, Qt.AlignCenter, label)
